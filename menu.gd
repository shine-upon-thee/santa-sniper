
extends Polygon2D

var SSniper

func _ready():
	SSniper = get_node("/root/SantaSniper")
	Input.warp_mouse_pos(Vector2(960,400))
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_node("player_name").set_text(SSniper.player_name)
	get_node("last_score").set_text(str(SSniper.last_score))
#	set_process_input(true)


func _input(ev):
	if Input.is_action_pressed("quit"):
		get_tree().quit()


func _on_quit_pressed():
	get_tree().quit()


func start_game(minutes):
	SSniper.minutes = minutes
	SSniper.goto_scene("game")


func _on_player_name_text_changed(text):
	SSniper.player_name = text.strip_edges()
	if SSniper.player_name == "":
		SSniper.player_name = "AnonymousHumbug"
