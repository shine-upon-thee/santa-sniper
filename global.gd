extends Node

var game_release_number = 1.2 # must be float for remote db storage

var debug = true

var minutes = 3

# save file stuff
var high_scores = []
var player_name = "AnonymousHumbug"
var save_file = "user://save.dat"
var last_score = 0 # TEMPORARY

##################################################################
# start scene switcher
var current_scene = null
func goto_scene(scene):
	scene = "res://" + scene + ".xscn"
	call_deferred("_swap_scene", scene)
func _swap_scene(scene):
	var s = ResourceLoader.load(scene)
	current_scene.free()
	current_scene = s.instance()
	get_tree().get_root().add_child(current_scene)
func _ready():
	load_data()
	var root = get_tree().get_root()
	current_scene = root.get_child( root.get_child_count() -1 )
# end scene switcher
##################################################################

##################################################################
# start debug print
func dprint(msg):
	if debug:
		print(msg)
# end debug print
##################################################################


##################################################################
# start save files
func save_data():
	var f = File.new()
	f.open(save_file, File.WRITE)
	if f.is_open():
		f.store_var(player_name)
		f.store_var(high_scores)
		f.close()
		print("Data saved.")
	else:
		print("Unable to open save file for writing!")

func load_data():
	var f = File.new()
	if f.file_exists(save_file):
		print("Opening save file...")
		f.open(save_file, File.READ)
		if f.is_open():
			player_name = f.get_var()
			high_scores = f.get_var()
			f.close()
			print("Data loaded.")
		else:
			print("Unable to open save file!")
	else:
		print("Save file does not exist.")
# end save files
##################################################################

##################################################################
# start high-score chart
func update_high_scores(score):
	last_score = score # TEMPORARY
	var new_score = {
		"name": player_name,
		"length": minutes,
		"score": score
	}
	high_scores.push_back(new_score)
	# somehow sort high scores
	# prune array to only 20 entries max
	# save to file
	save_data()
# end high-score chart
##################################################################

##################################################################
# start online leaderboard
func submit_online_score(score):
	var obj = {
		"action": "santasniper_submit_score",
		"name": player_name,
		"minutes": minutes,
		"score": score,
		"game_version": game_release_number,
		"os": OS.get_name(),
		"magic": "devel"
	}
	var result_json = http_get_post_url("post","www.shineuponthee.com","/game-json.pl", "obj="+obj.to_json())
	var result = {}
	result.parse_json(result_json)
	return result

func determine_score_rank(score):
	var obj = {
		"action": "santasniper_get_rank",
		"minutes": minutes,
		"score": score,
		"game_version": game_release_number
	}
	var result_json = http_get_post_url("post","www.shineuponthee.com","/game-json.pl", "obj="+obj.to_json())
	var result = {}
	result.parse_json(result_json)
	return result

func retrieve_online_scores(id=0):
	var obj = {
		"action": "santasniper_leaderboard",
		"minutes": minutes,
		"score_id": id,
		"game_version": game_release_number
	}
	var result_json = http_get_post_url("post","www.shineuponthee.com","/game-json.pl", "obj="+obj.to_json())
	var result = {}
	result.parse_json(result_json)
	return result
# end online leaderboard
##################################################################

##################################################################
# start http client
var timeout = 10000
var send_header = [
	"User-Agent: Santa Sniper/" + str(game_release_number) + " (SUT)",
	"Accept: */*",
	"content-type:application/x-www-form-urlencoded;charset=utf-8"
]
func http_get_post_url(method, host, url, body=""):
# this will get or post data from / to a host and url
	var poll_interval = 500
	var http = HTTPClient.new()
	# determine which method to use
	if method.to_upper() == "GET":
		method = http.METHOD_GET
	elif method.to_upper() == "POST":
		method = http.METHOD_POST
	else:
		dprint("HTTP: no method specified")
		return null
	# initiate connection
	var err = http.connect(host,80)
	if err:
		dprint("HTTP: failed to open a connection")
		return null
	# wait for a connection
	var time_elapsed = 0
	while (http.get_status() == http.STATUS_CONNECTING or http.get_status() == http.STATUS_RESOLVING):
		time_elapsed += poll_interval
		if time_elapsed > timeout:
			break
		http.poll()
		OS.delay_msec(poll_interval)
	# ensure we got a connection
	if (http.get_status() != http.STATUS_CONNECTED):
		dprint("HTTP: failed to connect to host")
		return null
	# try to get the url
	err = http.request(method,url,send_header,body)
	if err:
		dprint("HTTP: failed to send request")
		return null
	# wait while request is processed
	time_elapsed = 0
	while (http.get_status() == http.STATUS_REQUESTING):
		time_elapsed += poll_interval
		if time_elapsed > timeout:
			break
		http.poll()
		OS.delay_msec(poll_interval)
	# make sure there were no problems
	if (http.get_status() != http.STATUS_BODY and http.get_status() != http.STATUS_CONNECTED):
		dprint("HTTP: failed during request"+str(http.get_status()))
		return null
	# make sure there was a response
	if not http.has_response():
		dprint("HTTP: no response")
		return null
	# we should be good... let's populate some shit
	var headers = http.get_response_headers_as_dictionary()
	var code = http.get_response_code()
	var body = RawArray()
	time_elapsed = 0
	while (http.get_status() == http.STATUS_BODY):
		time_elapsed += poll_interval
		if time_elapsed > timeout:
			dprint("HTTP: timeout receiving")
			return null
		http.poll()
		var chunk = http.read_response_body_chunk()
		# nothing received, wait a little longer
		if chunk.size() == 0:
			OS.delay_usec(poll_interval)
		else:
			body = body + chunk
	return body.get_string_from_ascii()
# end http client
##################################################################
