SANTA SNIPER
------------
By: Shine Upon Thee

An entry for the Godot Christmas Game Jam 2014

In this game, you play as a Humbug, a trained elite sniper who is trying to kill off all of the Santa clones the government made using cells from a cryogenically-frozen Santa Claus who initially died in 1984 of a gunshot wound when he was mistaken as a home invader by a self-labeled cowboy in Texas. Or something.


All art assets are believed to be public domain.
All code released under the MIT license.
