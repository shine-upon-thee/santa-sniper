
extends Control

# TODO LIST
# fix issue with server timeouts:
#ERROR: submit_online_score: Invalid type in function 'parse_json' in base 'Dictionary'. Cannot convert argument 1 from Nil to String.
#   At: res://global.gdc:101.
#ERROR: submit: Invalid call. Unexisting function 'has' in base 'Nil'.
#   At: res://leaderboard.gdc:72.
# (think I fixed it) fix ERROR: connect: Attempt to connect to unexisting signal: _killed
# no santas were harmed in the making of this game. *fade* because santa is not real. the end.
# fix santa8 animation, walk between presents
# local high score chart?
# input player name (make better)
# minimum game version on server-side
# cycle leaderboard scores over time?
# add instructions
# add credits
# add server code?
# santa's eye is green on title screen
# add timestamp to leaderboards
# average points per minute on leaderboards
# shorten skateboard sound
# different sounds for the three skybound santas
# better positional audio (tweak min distance)
# bigger explosions
# shoot the star, put out the lights on the tree, zap sound
# multiply score by number of simultaneously-killed targets (doesn't seem to work anyhow)
# clock/timer icon?
# reindeer??
# blood??
# snowman should explode into snowflakes...
# maybe change reloading mechanic from automatic to manual? right-click to insert shell, wheel down-up to cock hammer?

# NOTES
# 225 rounds of ammo will show at the bottom of the screen easily

var screen_center # center of the screen
var reloading # essentially a semaphore
var score # user's score
var ammo # number of rounds left
var ammo_count = 6 # this is how many rounds of ammo you get multiplied by the number of minutes in the round
var minutes
var time_fraction # 1.0 = full time remaining, 0.0 = no time remaining
var recoil = 75 # pixels to jump when recoil kicks in
var music_voice
var game_over
var SSniper # globals

func init_game():
	game_over = false
	ammo = ammo_count * minutes
	score = 0
	get_node("overlay/score").set_text(str(score))
	reloading = false
	get_node("overlay/ammo").set_region_rect(Rect2(0,0,ammo*16,64))
	get_node("game_timer").set_wait_time(60*minutes)
	get_node("game_timer").start()
	get_node("wave_timer").start()
	for light in get_node("/root/game/borders").get_children():
		light.get_node("anim").set_speed(0.2)
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	for child in get_node("overlay").get_children():
		 child.show()
	Input.warp_mouse_pos(screen_center)
	music_voice = get_node("music").play("music1")
	get_node("music").set_pitch_scale(music_voice, 1.0)
	# loop through the targets and reset animations
	for target in get_node("targets").get_children():
		if target extends AnimatedSprite:
			if "ready" in target:
				target.ready = true
			# get the animation player name for the target
			var animname
			var targname = target.get_name()
			if targname.begins_with("santa"):
				animname = targname.replace("santa","anim")
			else:
				animname = "anim-" + targname
			# if the animation player exists
			if has_node("targets/"+animname):
				var anim = get_node("targets/"+animname)
				anim.stop_all()
				anim.play("reset")
			# hide presents
			if targname.begins_with("present"):
				target.hide()
			# reset stocking
			elif targname.begins_with("stocking"):
				target.reset()
	# reset the chimney santa
	get_node("targets/santa2/anim").play("reset")
	get_tree().set_pause(false)


func _ready():
	SSniper = get_node("/root/SantaSniper")
	minutes = SSniper.minutes
	screen_center = get_tree().get_root().get_size_override() / 2
	# loop through the targets and connect signals
	for target in get_node("targets").get_children():
		if target extends AnimatedSprite:
			target.connect("_killed",self,"target_killed",[target])
			# get the animation player name for the target
			var animname
			var targname = target.get_name()
			if targname.begins_with("santa"):
				animname = targname.replace("santa","anim")
			else:
				animname = "anim-" + targname
			# if the animation player exists
			if has_node("targets/"+animname):
				var anim = get_node("targets/"+animname)
				# connect signal for when target escapes unharmed
				# it appears this signal is triggered only after both reset and the queued animation stop, which is good.
				anim.play("reset")
				if not anim.is_connected("finished",self,"target_escaped"):
					anim.connect("finished",self,"target_escaped",[target])
	set_process_input(true)
	randomize()
	init_game()


func _input(ev):
	# quit back to main menu if user presses Escape
	if Input.is_action_pressed("quit"):
		SSniper.goto_scene("menu")
	if (ev.type == InputEvent.MOUSE_MOTION):
		# update custom cursor position
		get_node("overlay/cursor").set_pos(ev.pos)
		# recalculate sound position
		# maybe this would be done better/automatically with a camera and sampleplayer2d?
		var snd_pos = (ev.pos - screen_center) / screen_center
		get_node("overlay/cursor/sfx").set("default/pan", snd_pos.x)
		get_node("overlay/cursor/sfx").set("default/height", snd_pos.y)
		# debug label
#		get_node("overlay/debug").set_text(str(ev.pos) + " - " + str(get_node("game_timer").get_time_left()))
		
	elif (ev.type == InputEvent.MOUSE_BUTTON and ev.pressed and ev.button_index == BUTTON_LEFT and !reloading and ammo):
#		print("shooting" + str(reloading))
		reloading = true
		ammo -= 1
		get_node("overlay/ammo").set_region_rect(Rect2(0,0,ammo*16,64))
		if ammo:
			get_node("overlay/cursor/anim").play("shoot")
		else:
			get_node("overlay/cursor/anim").play("last-shot")
		var new_pos = Vector2((randi() % recoil) - recoil / 2 - 1, (randi() % recoil) - recoil / 2 - 1)
		Input.warp_mouse_pos(get_global_mouse_pos()+new_pos)
#		Input.warp_mouse_pos(ev.pos+new_pos)
	elif (ev.type == InputEvent.MOUSE_BUTTON and ev.pressed and ev.button_index == BUTTON_LEFT and !reloading):
		get_node("overlay/cursor/sfx").play("dry-fire")
		# user has no ammo and has noticed. in case there is more than 3 seconds left, let's speed it up
		var time_left = get_node("game_timer").get_time_left()
		print("time left", time_left)
		if time_left > 3:
			game_over = true
			get_node("wave_timer").stop()
			get_node("game_timer").set_wait_time(5)
			get_node("game_timer").start()
	# this stuff is possibly to be implemented later as a separate difficulty mode, where you have to manually reload
	elif (ev.type == InputEvent.MOUSE_BUTTON and ev.pressed and ev.button_index == BUTTON_RIGHT and reloading and ammo):
		# insert shell if we have ammo
		# play sound
		# set a reload_state variable
		pass
	elif (ev.type == InputEvent.MOUSE_BUTTON and ev.pressed and ev.button_index == BUTTON_WHEEL_DOWN and reloading):
		# pull hammer back
		# set a reload_state variable
		pass
	elif (ev.type == InputEvent.MOUSE_BUTTON and ev.pressed and ev.button_index == BUTTON_WHEEL_UP and reloading):
		# lock it
		#reloading = false
		pass


func _on_wave_timer_timeout():
	# calculate the time
	var time_left = get_node("game_timer").get_time_left()
	time_fraction = time_left / (60 * minutes)
	var wave_delay = 9 * (time_fraction / 2) + 1
	get_node("wave_timer").set_wait_time(wave_delay)
#	print(wave_delay)
#	get_node("overlay/debug").set_text(str(time_fraction))
	# speed up music
	get_node("music").set_pitch_scale(music_voice, 2.0 - time_fraction)
	# speed up the light border
	var light_speed = 10 - 10 * time_fraction
	for light in get_node("/root/game/borders").get_children():
		light.get_node("anim").set_speed(light_speed)
	# send out a wave
	send_wave(time_fraction)


func send_wave(fraction):
	# loop through targets and determine which are ready to send
	var ready_targets = []
	for target in get_node("targets").get_children():
		var targname = target.get_name()
		if target extends AnimatedSprite and not targname.begins_with("present"):
			# assume ready
			if "ready" in target and target.ready:
				ready_targets.push_back(target)
			# santa3 can only be sent if all presents are claimed
			if targname == "santa3" and target.ready:
				for x in range(4):
					if not get_node("targets/present"+str(x+1)).ready:
						ready_targets.resize( ready_targets.size() - 1 )
						break
	# choose one random target and send it out, if any are ready
	if ready_targets.size() > 0:
		var target = ready_targets[randi() % ready_targets.size()]
		var targname = target.get_name()
		print("Sending " + targname)
		# get the animation player name for the target
		var animname
		if targname.begins_with("santa"):
			animname = targname.replace("santa","anim")
		else:
			animname = "anim-" + targname
#				target.ready = false
		# if the animation player exists
		if has_node("targets/"+animname):
			var anim = get_node("targets/"+animname)
			target.ready = false
			# randomize animation
			var anim_count = anim.get_animation_list().size() - 1
			var rand_anim = 1 + randi() % anim_count
			anim.play("reset")
			anim.queue(str(rand_anim))
			target.get_node("anim").play("go")
		elif targname == "santa2": #santa2 doesn't move, therefore doesn't have separate animationplayer
			target.ready = false
			target.get_node("anim").play("go")

func target_escaped(target):
	target.ready = true
	target.get_node("anim").stop()

func target_killed(target):
# this function is called when a target is killed
	# if killed target had a score value, add it
	if "points_val" in target:
		score += round(target.points_val * (1 + (minutes - minutes * time_fraction)))
		get_node("overlay/score").set_text(str(score))
	# if killed target had an ammo value, add it
	if "ammo_val" in target:
		ammo += target.ammo_val
		get_node("overlay/ammo").set_region_rect(Rect2(0,0,ammo*16,64))
	# stop the appropriate controlling animationplayer
	var targname = target.get_name()
	var anim
	if targname.begins_with("santa"):
		anim = targname.replace("santa","anim")
	else:
		anim = "anim-" + targname
	if has_node("targets/"+anim):
		get_node("targets/"+anim).stop()


############################################################
# END GAME FUNCTIONS

func _on_game_timer_timeout():
	# ran out of time, give a few more seconds before popping up the game over screen
	if !game_over:
		game_over = true
		get_node("wave_timer").stop()
		get_node("game_timer").set_wait_time(5)
		get_node("game_timer").start()
		get_node("overlay/cursor/sfx").play("alarm")
	else: # game is over for real now
		score += ammo * 20 # ammo bonus points
		get_tree().set_pause(true)
		for child in get_node("overlay").get_children():
			 child.hide()
		get_node("leaderboard").end_game(score)
#		SSniper.goto_scene("menu")

