
extends AnimatedSprite

# this script is used by all the santas (and the snowman and the soldier and the presents)
# probably should have renamed it to targets.gd

export var ready = false # is this target ready to be deployed?
export var points_val = 50 # points gained by user by killing this target
export var ammo_val = 0 # ammo gained by user by killing this target

func _ready():
	add_user_signal("_killed")

func _on_kill_pressed():
	emit_signal("_killed")
	get_node("anim").play("kill")
	get_node("anim").connect("finished",self,"kill_complete",[],CONNECT_ONESHOT)

func kill_complete():
	ready = true
	get_node("sfx").stop_all()