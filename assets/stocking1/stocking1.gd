
extends AnimatedSprite

export var points_val = 200

func _ready():
	add_user_signal("_killed")
	reset()
	
func reset():
	get_node("anim").stop()
	get_node("pop").set_disabled(false)
	get_node("kill").set_disabled(true)


func _on_kill_pressed():
	emit_signal("_killed")
	get_node("anim").play("kill")
