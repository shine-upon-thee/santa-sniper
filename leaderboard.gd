
extends Control

var score_id
var score_rank
var score
var grid = Vector2(3,10) # columns and rows of leaders

var SSniper

func _ready():
	hide()
	SSniper = get_node("/root/SantaSniper")
#	get_node("retry").hide()
	#get_node("retry").show()
#	var update_thread = Thread.new()
#	update_thread.start(self,"end_game",123123)
	#submit()
	var entry
	var prev = get_node("rank1")
	var offset = Vector2(350,55)
	var initx = (get_tree().get_root().get_size_override().x - (offset.x * grid.x)) / 2
	prev.set_pos( Vector2(initx, 330))
	for i in range(2,(grid.x*grid.y)+1):
		entry = prev.duplicate()
		if i % int(grid.y) == 1:
			entry.set_pos(prev.get_pos() + Vector2(offset.x,-offset.y*(grid.y-1)))
		else:
			entry.set_pos(prev.get_pos() + Vector2(0,offset.y))
		entry.set_name("rank"+str(i))
		add_child(entry)
		prev = entry
	# for testing purposes
	#end_game(201)


#func _on_player_name_text_changed():
#	if SSniper.player_name == "":
#	SSniper.player_name = get_node("player_name").get_text().strip_edges().replace("\n", "").replace("\t", "")
#		SSniper.player_name = "AnonymousHumbug"

func _on_hack_timeout():
	submit()
#	rank()
#	leaders()

func end_game(final_score):
	show()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	# copy to member var
	score = final_score
	get_node("title").set_text("TOP " + str(grid.x*grid.y) + " LEADERS: (" + str(SSniper.minutes)+"-Minute Game)")
	status("Final score: " + str(score))
	get_node("match_score").set_text(str(score))
	SSniper.update_high_scores(score) # local - not really used, maybe fallback or toggle option?
#	submit(score)
	# need a hack until we use threads
	get_node("hack").start()

func status(msg):
	get_node("retry").hide()
	get_node("quit").hide()
	get_node("again").hide()
	get_node("oldstatus").set_text( get_node("status").get_text() )
	get_node("status").set_text(msg)

func submit():
	# submit
	status("Submitting score...")
	var result = SSniper.submit_online_score(score)
	if result.has("error"):
#		status("Error submitting score. Please try again.")
		status(result.msg)
		get_node("retry").connect("pressed",self,"submit")
		get_node("retry").show()
		get_node("quit").show()
		get_node("again").show()
	elif result.has("id"):
		status("Score sucessfully submitted to leaderboard!")
		score_id = result["id"]
		# now get the rank
		rank()

func rank():
	# rank
	status("Fetching score rank...")
	var result = SSniper.determine_score_rank(score)
	if result.has("error"):
#		status("Error determining rank. Please try again.")
		status(result.msg)
		get_node("retry").connect("pressed",self,"rank")
		get_node("retry").show()
		get_node("quit").show()
		get_node("again").show()
	elif result.has("rank"):
		score_rank = result["rank"]
		status("Rank sucessfully determined!")
		get_node("match_rank").set_text(str(score_rank))
		# now do the leaderboard
		leaders()

func leaders():
	# leaderboard
	status("Fetching top of the leaderboard...")
	var scores = SSniper.retrieve_online_scores(score_id)
	if scores.has("error"):
#		status("Error fetching leaderboard. Try again later.")
		status(scores.msg)
		get_node("retry").connect("pressed",self,"leaders",[score_id])
		get_node("retry").show()
	else:
	# TODO parse leaders and print to screen
		status("Successfully fetched leaderboard!")
		#print(scores)
		if scores.has("scores"):
			for i in range(1,(grid.x*grid.y)+1):
				get_node("rank"+str(i)).set_text(str(i))
				var entry = str(i-1) # start count at 0
				if scores["scores"].has(entry):
					get_node("rank"+str(i)+"/name").set_text(scores["scores"][entry].name)
					get_node("rank"+str(i)+"/score").set_text(scores["scores"][entry].score)
					if scores["scores"][entry].id == score_id:
						get_node("rank"+str(i)+"/anim").play("blink")
					elif get_node("rank"+str(i)+"/anim").is_playing():
						get_node("rank"+str(i)+"/anim").play("reset")
		else:
			status("No scores in leaderboard!")
	get_node("quit").show()
	get_node("again").show()



func _on_quit_pressed():
	SSniper.goto_scene("menu")

func _on_again_pressed():
	hide()
	get_node("/root/game").init_game()